import uvicorn

from fastapi import FastAPI


app = FastAPI()


@app.get("/items")
async def get_items():
    return {"Hello": "world"}


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=int(os.getenv("PORT", 8000)))
