from setuptools import setup, find_packages

setup(
    name='fastapi-demo',
    version='1.0',
    packages=find_packages(),
    setup_requires=['pytest-runner'],
    tests_require=['pytest']
)
